## Changelog

### 1.1

#### 1.1.1 [2020-20-26]

* Version 1.1.1
* Another Typo, unbelievable

#### 1.1.0 [2020-10-26]

* Version 1.1.0
* Small tweaks in readme
* Added configuration variable DEPARTMENT
* Removed .gitignore from .gitignore
* Added update check
* Changed startup console log text
* **[BUG]** title was not rendered
* **[BUG]** title was not string
* Remove sport and kultur links to avoid double postings by their respective rss bots
* Added +++ to status title
* Added .gitignore to .gitignore

---
### 1.0

#### 1.0.3 [2020-10-24]

* Version 1.0.3
* ANOTHER TYPO?!?!

#### 1.0.2 [2020-10-24]

* Version 1.0.2
* I hate readme files

#### 1.0.1 [2020-10-24]

* Version 1.0.1
* Forgot one step in installation instruction, d'err

#### 1.0.0 [2020-10-24]

* Version 1.0.0
* Updated Readme
* The Bot
* Added example service file
* Exclude timestamp file from versioning
* Added READ_INTERVAL and some comments to .env
* Require fs
* .env-sample
* .gitignore
* package.json
* ReadMe
* License