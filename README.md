# PMJ SRF News Rss Bot (for Mastodon)

This Bot reads the news feed from swiss national broadcaster [SRF](https://www.srf.ch)  
You can see the Bot in action on [@srfnewsrss](https://social.pmj.rocks/@srfnewsrss), [@srfsportrss](https://social.pmj.rocks/@srfsportrss) and [@srfkulturrss](https://social.pmj.rocks/@srfkulturrss)

Currently the Bot is programmed to handle the formatting of the SRF RSS Feed but it can also be used as a blueprint and can be adjusted to toot other RSS feeds ;)

It uses the [mastodon-api](https://github.com/vanita5/mastodon-api) node package by Vanita5

## Installation

* Download the repo  
  ```
  git clone https://gitlab.com/pmj-mastodon/srfnewsrss.git  
  ```
* Install dependencies
  ```
  cd srfnewsrss
  npm install
  ```
* Rename `.env-sample` to `.env`  
  ```
  mv .env-sample .env
  ```
* Adjust settings in `.env`
* Start the bot either from the console...  
  ```
  nodejs bot.js
  ```
* ...or adjust the `pmj-srfnewsrss.service.example`  
  move it to `/etc/systemd/system/pmj-srfnewsrss.service`
  ```
  sudo cp pmj-srfnewsrss.service.example /etc/systemd/system/pmj-srfnewsrss.service
  ```
  reload the systemd daemon with
  ```
  sudo systemctl daemon-reload
  ```
  and enable and start the bot with
  ```
  sudo systemctl enable pmj-srfnewsrss
  sudo systemctl start pmj-srfnewsrss
  ```

## History

Since Twitter abruptly banned my account for alleged misuse of their API on October 13th 2020, I was cut off from my primary source of news for Switzerland.  
And since I like to have everything in one place, I decided to create a small Bot which fetches the official RSS feed and turns it into neat Mastodon toots.