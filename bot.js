require('dotenv').config();

const https = require('https');
const xml2js = require('xml2js');
const mastodonapi  = require('mastodon-api');
const fs = require('fs');

const parser = new xml2js.Parser();
const mastodon  = new mastodonapi({
  access_token: process.env.ACCESS_TOKEN,
  timeout_ms: 60*1000,
  api_url: `${process.env.INSTANCE}`,
});

const timestampfile = 'timestamp';
const imgfile = 'img.jpg';
const interval = process.env.READ_INTERVAL * 1000;
const department = process.env.DEPARTMENT;

console.log('PMJ RSS Bot started...');

// download image
function downloadImage(url, callback) {
  https.get(url, function(response) {
    let imageData = '';
    response.setEncoding('binary');
    response.on('data', function(chunk) {
      imageData += chunk;
    });
    response.on('end', function() {
      fs.writeFileSync(imgfile, imageData, 'binary');
      let img = fs.createReadStream(imgfile);
      callback(img);
    });
  });
}

// get and sort rss items
function getRSSItems(callback) {
  /**
  * SRF News returns:
  *
  * title
  * link
  * description
  * guid
  * pubDate
  **/
  
  // read rss feed
  https.get(`${process.env.FEED_URL}`, function(feed) {
    let data = '';
    feed.on('data', function(stream) {
      data += stream;
    });
    feed.on('end', function() {
      try {
        parser.parseString(data, function(error, result) {
          if(error === null)
          {
            let lastpost = new Date(0);
            let date = new Date(0);
            
            // set items
            let items = result.rss.channel[0].item;
            // sort items by date
            items.sort(function(a,b) {
              return new Date(b.pubDate[0]) - new Date(a.pubDate[0]);
            });
            // remove sport and kultur links if the bot is reading the main news
            if (department === 'news') {
              let sportkultur = /(srf\.ch\/sport|srf\.ch\/kultur)/g;
              items = items.filter((item) => {
                return !item.link[0].match(sportkultur)
              });
            }
            callback(items);
          }
          else
          {
            callback(error);
          }
        });
      } catch (e) {
        console.error(e);
      }
    });
  });
}

// process items to add update check
function processItems(callback) {
  /**
  * SRF News returns:
  *
  * title
  * link
  * description
  * guid
  * pubDate
  **/
  
  getRSSItems(function(items) {
    // creat an array of links to check for updates
    let originals = items;
    // remove duplicates
    items = items.filter((item, index, self) =>
      index === self.findIndex((i) => (
        i.title[0] === item.title[0]
      ))
    );
    // return only one item upon first start
    if (fs.existsSync(timestampfile) === false)
    {
      items.length = 1;
    }
    // always start with the oldest post
    items.reverse();
    
    callback({items: items, originals: originals});
  });
}

// toot item
function toot(item,update) {
  // because the image is inside the description i need to separate them
  // get image url from description
  let regex_image = /http(.*?)[^"]+/g;
  let url = item.description[0].match(regex_image)[0];
  // get actual description from description
  let regex_status = /(?<=>)(.*?)+/g;
  let msg = item.description[0].match(regex_status)[0];
  
  let title = '';
  if (update) {
    title += `+++ [UPDATE] ${item.title[0]} +++`;
  } else {
    title += `+++ ${item.title[0]} +++`;
  }
  
  downloadImage(url, async function(img) {
    // media params
    let params_media = {
      file: img,
      description: ''
    }
    
    // post the media
    let response_media = await mastodon.post('api/v1/media', params_media);
    
    // get media id
    let media_id = response_media.data.id;
    
    // status params
    let params_status = {
      status: `${title}\n\n${msg}\n\n${item.link[0]}\n`,
      media_ids: [media_id]
    }
    
    // post status
    let response_status = await mastodon.post('api/v1/statuses', params_status);
    fs.unlinkSync(imgfile);
    console.log(`TOOT: ${item.title[0]}\n\n${msg}\n\n${item.link[0]}\n`);
  });
}

// main
function readRSS()
{
  // process items
  processItems(function(rss) {
    let lastpost = new Date(0);
    let date = new Date(0);
    let links = [];
    rss.originals.forEach( function(original) {
      // if this link is not yet indexed, add it with a count of 1, else count up
      if (typeof links[original.link[0]] === 'undefined')
      {
        links[original.link[0]] = 1;
      }
      else
      {
        links[original.link[0]]++;
      }
    });
    rss.items.forEach( function(item)
    {
      date = new Date(item.pubDate[0]);
      // check if timestamp backup file exists
      try {
        // timestamp backup exists
        if (fs.existsSync(timestampfile))
        {
          lastpost = new Date(fs.readFileSync(timestampfile, 'utf8'));
        }
        if (date > lastpost)
        {
          toot(item,links[item.link[0]] > 1);
          fs.writeFileSync(timestampfile, date);
        }
      } catch(err) {
        console.error(err);
      }
    });
  });
  
}

readRSS();
setInterval(readRSS, interval);